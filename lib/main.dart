import 'package:flutter/material.dart';
void main() {
  runApp(weatherApp());
}

class weatherApp extends StatefulWidget {
  @override
  State<weatherApp> createState() => _weatherAppState();
}

class _weatherAppState extends State<weatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/images/w1.png'),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: ListView(
          children: [
            Container(
              // color: Colors.amber,
                child: Column(
                  children: [
                    Container(
                      // color: Colors.pink,
                        width: 250,
                        height: 70,
                        // alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text(
                                  "เมืองชลบุรี",
                                  style: TextStyle(
                                    fontSize: 40,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                    Container(
                      // color: Colors.black,
                      alignment: Alignment.bottomCenter,
                      child: const Text(" 30°",
                          style: TextStyle(
                            fontSize: 85,
                            color: Colors.white,
                          )),
                    ),
                    Container(
                      // color: Colors.black,
                      alignment: Alignment.bottomCenter,
                      child: const Text("แดดจัดเป็นส่วใหญ่",
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,)),
                    ),
                    Container(
                      // color: Colors.black,
                      alignment: Alignment.bottomCenter,
                      child: const Text("สูงสุด: 31° ต่ำสุด: 24°",
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,)),
                    ),
                    Container(
                      // color: Colors.amber,
                      height: 50,
                    ),

                  ],
                )
            ),
            Container(
              color: Colors.indigo.withOpacity(0.2),
              child: Center(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      for (int i = 1; i <= 3; i++) buildDayTimeIcon(i),
                      for (int i = 4; i <= 10; i++) buildDayTime2Icon(i)
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 20,
            ),
            Container( //จ-ศ
              color: Colors.indigo.withOpacity(0.2),
              child: Column(
                children: [
                  buildWeatherDay1Icon(),
                  buildWeatherDay2Icon(),
                  buildWeatherDay3Icon(),
                  buildWeatherDay4Icon(),
                  buildWeatherDay5Icon(),
                  buildWeatherDay6Icon(),
                  buildWeatherDay7Icon(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildDayTimeIcon(var i) {
  if (i < 4) {
    i = "0$i";
  }
  return Padding(
    padding: const EdgeInsets.all(5.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "30°",
            style: TextStyle(color: Colors.white, fontSize: 19),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Icon(
            Icons.sunny,
            // WeatherIcons.night_snow,
            // color: Colors.white,
            color: Colors.amber,
            size: 30,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "$i:00",
            style: TextStyle(color: Colors.white, fontSize: 19),
          ),
        ),
        Container(
          height: 10,
        )
      ],
    ),
  );
}

Widget buildDayTime2Icon(var i) {

  if (i < 10) {
    i = "0$i";
  }
  return Padding(
    padding: const EdgeInsets.all(5.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "22°",
            style: TextStyle(color: Colors.white, fontSize: 19),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Icon(
            Icons.cloud,
            // WeatherIcons.night_snow,
            // color: Colors.white,
            color: Colors.white38,
            size: 30,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "$i:00",
            style: TextStyle(color: Colors.white, fontSize: 19),
          ),
        ),
        Container(
          height: 10,
        )
      ],
    ),
  );
}

Widget buildWeatherDay1Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "วันนี้ ศ.",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.sunny,
          color: Colors.amber,
          size: 30,
        ),
        Text(
          "แดดจัดเป็นส่วนใหญ่",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "24°/31°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay2Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "  ส. ",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.cloud,
          color: Colors.white38,
          size: 30,
        ),
        Text(
          "เมฆมาก",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "20°/24°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay3Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "  ส. ",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.cloudy_snowing,
          color: Colors.blue,
          size: 33,
        ),
        Text(
          " ฝนตก ",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "18°/21°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay4Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "  อา. ",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.sunny,
          color: Colors.amber,
          size: 30,
        ),
        Text(
          " แดดออก",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "26°/30°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay5Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "จ.",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.cloudy_snowing,
          color: Colors.blue,
          size: 33,
        ),
        Text(
          " ฝนตก ",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "18°/21°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay6Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "  อ.  ",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.sunny,
          color: Colors.amber,
          size: 30,
        ),
        Text(
          "  แดดออก",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "26°/30°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

Widget buildWeatherDay7Icon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "  พ. ",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Icon(
          Icons.cloud,
          color: Colors.white38,
          size: 30,
        ),
        Text(
          "เมฆมาก",
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),

        Text(
          "20°/24°",
          style: TextStyle(
              color: Colors.white, fontSize: 20),
        ),
      ],
    ),
  );
}

